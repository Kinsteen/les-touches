package fr.kinsteen.lestouches;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.UnsupportedAudioFileException;

public class SoundManager {
	
	/**
	 * This function takes a VORBIS encoded ogg audio stream, and converts it in a PCM (Wav format) audio stream.
	 * @param audioInputStream
	 * @return
	 */
	private static AudioInputStream convertToPCM(AudioInputStream audioInputStream) {
        AudioFormat m_format = audioInputStream.getFormat();

        if ((m_format.getEncoding() != AudioFormat.Encoding.PCM_SIGNED) &&
            (m_format.getEncoding() != AudioFormat.Encoding.PCM_UNSIGNED))
        {
            AudioFormat targetFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED,
                m_format.getSampleRate(), 16,
                m_format.getChannels(), m_format.getChannels() * 2,
                m_format.getSampleRate(), m_format.isBigEndian());

            return AudioSystem.getAudioInputStream(targetFormat, audioInputStream);
        }

        return audioInputStream;
	}
	
	public static Mixer.Info getCableInput() {
		Mixer.Info[] infos = AudioSystem.getMixerInfo();
		Mixer.Info mixer = null;
		
		for (Mixer.Info info : infos) {
			if (info.getName().equalsIgnoreCase("Cable Input (VB-Audio Virtual Cable)")) {
				return info;
			}
		}
		
		return mixer;
	}
	
	public static Clip loadClip(String filename) {
		Clip clip = null;
		File file = new File(filename);
		
		try {
			if (file.exists()) {
				System.out.println("Loading new Clip in memory...");
				AudioInputStream sound = convertToPCM(AudioSystem.getAudioInputStream(file));
				// load the sound into memory (a Clip)
				clip = AudioSystem.getClip(getCableInput());
				clip.open(sound);
			} else {
				System.err.println("File not found : " + filename);
				return null;
	        }
		} catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (LineUnavailableException e) {
			e.printStackTrace();
		}
		
		return clip;
	}
}
