package fr.kinsteen.lestouches;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sound.sampled.Clip;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.Mixer;

import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;
import org.jnativehook.mouse.NativeMouseEvent;
import org.jnativehook.mouse.NativeMouseInputListener;
import org.jnativehook.mouse.NativeMouseWheelEvent;
import org.jnativehook.mouse.NativeMouseWheelListener;

public class LesTouches implements NativeMouseInputListener, NativeMouseWheelListener, NativeKeyListener {
	
	public static Mixer.Info cableInput;
	private static Map<String, Clip> clips = new HashMap<String, Clip>();
	private static Map<Clip, LineListener> listeners = new HashMap<Clip, LineListener>();
	private static ArrayList<String> modifiers = new ArrayList<String>();
	private static ArrayList<String> keyPressed = new ArrayList<String>();
	
	public void nativeMouseClicked(NativeMouseEvent e) {}

	public void nativeMousePressed(NativeMouseEvent e) {
		switch (e.getButton()) {
		case 1: // Clic gauche
			playSound("ClicGauche.ogg");
			break;
		case 2: // Clic droit
			playSound("ClicDroit.ogg");
			break;
		case 3: // Clic molette
			playSound("ClicMolette.ogg");
			break;
		case 4: // Précedent
			playSound("ClicPrec.ogg");
			break;
		case 5: // Suivant
			playSound("ClicSuiv.ogg");
			break;
			
		default:
			break;
		}
	}

	public void nativeMouseReleased(NativeMouseEvent e) {
		switch (e.getButton()) {
		case 1: // Clic gauche
			stopSound("ClicGauche.ogg");
			break;
		case 2: // Clic droit
			stopSound("ClicDroit.ogg");
			break;
		case 3: // Clic molette
			stopSound("ClicMolette.ogg");
			break;
		case 4: // Précedent
			stopSound("ClicPrec.ogg");
			break;
		case 5: // Suivant
			stopSound("ClicSuiv.ogg");
			break;
		}
	}
	
	public void nativeMouseMoved(NativeMouseEvent e) {}
 
	public void nativeMouseDragged(NativeMouseEvent e) {}
	
	public void nativeKeyPressed(NativeKeyEvent e) {
		String key = NativeKeyEvent.getKeyText(e.getKeyCode());
		/* 29 = ctrl
		 * 42 = shift
		 * 56 = alt
		 * 3638 = right shift
		 */
		
		if (!keyPressed.contains(key)) {
			if (e.getKeyCode() == 70) {
				System.out.println("Closing program !");
				try {
					GlobalScreen.unregisterNativeHook();
				} catch (NativeHookException e1) {
					e1.printStackTrace();
				}
				System.exit(1);
			}
			
			switch (e.getKeyCode()) {
				case 70: // Arret defil
					System.out.println("Closing program !");
					try {
						GlobalScreen.unregisterNativeHook();
					} catch (NativeHookException e1) {
						e1.printStackTrace();
					}
					System.exit(1);
					break;
				case 29:
					modifiers.add("Ctrl");
					keyPressed.add(key);
					break;
				case 42:
					modifiers.add("Shift");
					keyPressed.add(key);
					break;
				case 56:
					modifiers.add("Alt");
					keyPressed.add(key);
					break;
				case 3638:
					modifiers.add("RShift");
					keyPressed.add(key);
					break;
				default:
					String modifString = "";
					Collections.sort(modifiers); // Sort alphabetically, to have consistent naming
					for (String modif : modifiers) {
						modifString += modif + "+";
					}

					if (modifString != "") {
						playOnce(modifString + key + ".ogg");
					} else {
						playSound(key + ".ogg");
						keyPressed.add(key);
					}
					break;
			}
		}
	}

	public void nativeKeyReleased(NativeKeyEvent e) {
		String key = NativeKeyEvent.getKeyText(e.getKeyCode());
		stopSound(key + ".ogg");
		keyPressed.remove(key);
		
		switch (e.getKeyCode()) {
			case 29:
				modifiers.remove("Ctrl");
				break;
			case 42:
				modifiers.remove("Shift");
				break;
			case 56:
				modifiers.remove("Alt");
				break;
			case 3638:
				modifiers.remove("RShift");
				break;
			default:
				break;
		}
	}

	public void nativeKeyTyped(NativeKeyEvent arg0) {}

	public void nativeMouseWheelMoved(NativeMouseWheelEvent e) { // Need to add a sort of cooldown
		switch (e.getWheelRotation()) {
		case 1:
			if (clips.get("ScrollBas.ogg") == null) {
				playOnce("ScrollBas.ogg");
			} else {
				Clip clip = clips.get("ScrollBas.ogg");
				if (!clip.isActive()) {
					playOnce("ScrollBas.ogg");
				}
			}
			break;
		case -1:
			if (clips.get("ScrollHaut.ogg") == null) {
				playOnce("ScrollHaut.ogg");
			} else {
				Clip clip = clips.get("ScrollHaut.ogg");
				if (!clip.isActive()) {
					playOnce("ScrollHaut.ogg");
				}
			}
			break;

		default:
			break;
		}
	}
	
	private Clip playSound(String file, boolean once) {
		Clip clip = clips.get(file);
		if (clips.get(file) == null) {
			Clip newClip = SoundManager.loadClip(file);
			if (newClip != null) {
				clips.put(file, newClip);
				clip = newClip;
			} else {
				// Error is managed in loadClip()
				return null;
			}
		}
		
		clip.setFramePosition(0);
		clip.start();

		if (!once) {
			LineListener list = new LineListener() {
				public void update(LineEvent e) {
					Clip context = ((Clip) e.getSource());
					if (e.getType() == LineEvent.Type.STOP) {
						context.setFramePosition(0); // It's rewind time
						context.start();
					}
				}
			};
			
			clip.addLineListener(list);
			listeners.put(clip, list);
		}
		
		return clip;
	}

	/**
	 * Play on loop until stopped
	 * @param file
	 */
	private void playSound(String file) {
		playSound(file, false);
	}

	/**
	 * 
	 * Play once
	 * @param file
	 */
	private Clip playOnce(String file) {
		return playSound(file, true);
	}
	
	private void stopSound(String file) {
		Clip clip = clips.get(file);
		if (clips.get(file) != null) {
			clip.removeLineListener(listeners.get(clip));
		}
	}

	public static void main(String[] args) {
		cableInput = SoundManager.getCableInput();
		// Get the logger for "org.jnativehook" and set the level to off.
		Logger logger = Logger.getLogger(GlobalScreen.class.getPackage().getName());
		logger.setLevel(Level.OFF);

		// Don't forget to disable the parent handlers.
		logger.setUseParentHandlers(false);
		
		try {
			GlobalScreen.registerNativeHook();
		}
		catch (NativeHookException ex) {
			System.err.println("There was a problem registering the native hook.");
			System.err.println(ex.getMessage());

			System.exit(1);
		}

		// Construct the example object.
		LesTouches example = new LesTouches();

		// Add the appropriate listeners.
		GlobalScreen.addNativeMouseListener(example);
		GlobalScreen.addNativeMouseMotionListener(example);
		GlobalScreen.addNativeMouseWheelListener(example);
		GlobalScreen.addNativeKeyListener(example);
		System.out.println("Ready!");
	}
}